package unitTests;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;


public class SquashtmTest {
	
	private WebDriver driver;
	
	// r�cup�rer la partie variable de l'url lors du lancement du test sur jenkins puis la r�injecter dans le test
	private String url = "http://192.168.102.91:9876/squash/login";
	private String browser = System.getProperty("browser");
	private String user = "admin";
	private String pwd = "admin";
	
	@Before
	public void init() {
		
		DesiredCapabilities capabilities = new DesiredCapabilities();
		capabilities.setJavascriptEnabled(false);
		
		if(browser.equalsIgnoreCase("chrome")) {
			System.setProperty("webdriver.chrome.driver", "src/main/resources/chromedriver.exe");
			driver = new ChromeDriver(capabilities);
		} else if (browser.equalsIgnoreCase("firefox")) {
			System.setProperty("webdriver.gecko.driver", "src/main/resources/geckodriver.exe");
			FirefoxOptions options = new FirefoxOptions(capabilities);
			driver = new FirefoxDriver(options);
		}
		
		//options.setPageLoadStrategy(PageLoadStrategy.NONE);
		//options.addArguments("--disable-gpu");
		
		//System.setProperty("webdriver.gecko.driver", "src/main/resources/geckodriver.exe");
		//driver = new FirefoxDriver();
		driver.manage().window().maximize();
		WebDriverWait wdw = new WebDriverWait(driver, 10);
		//driver.manage().timeouts().pageLoadTimeout(40000, TimeUnit.MILLISECONDS);
		driver.get(url);
			
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		wdw.until(ExpectedConditions.elementToBeClickable(driver.findElement(By.xpath("/html/body/div[9]/div[1]/div/div/div[3]/button")))).click();
		
		//driver.findElement(By.tagName("body")).sendKeys("Keys.ESCAPE");
		
		
	}
	
	@After
	public void teardown() {
		driver.quit();
	}
	
	@Ignore
	public void testi() {
		
	}
	
	@Test
	public void test() throws Exception {
		
	}

}
