package unitTests;

import java.util.concurrent.TimeUnit;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.hamcrest.Condition.Step;
import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import pages.LoginPage;
import pages.MenuPage;
import pages.NewTestCasePopup;
import pages.TestCasePage;

public class CreerCasDeTestTest {
		
		private WebDriver driver;
		
		// r�cup�rer la partie variable de l'url lors du lancement du test sur jenkins puis la r�injecter dans le test
		private String url = "http://192.168.102.91:9876/squash/login";
		//private String browser = System.getProperty("browser");
		String browser = "firefox";
		private String user = "admin";
		private String pwd = "admin";
		
		private String projectName = "TM01_SQUASH_TM";
		private String tcNumber = "10";
		
		@Before
		public void init() {
			
			DesiredCapabilities capabilities = new DesiredCapabilities();
			capabilities.setJavascriptEnabled(false);
			
			if(browser.equalsIgnoreCase("chrome")) {
				System.setProperty("webdriver.chrome.driver", "src/main/resources/chromedriver.exe");
				driver = new ChromeDriver(capabilities);
			} else if (browser.equalsIgnoreCase("firefox")) {
				System.setProperty("webdriver.gecko.driver", "src/main/resources/geckodriver.exe");
				FirefoxOptions options = new FirefoxOptions(capabilities);
				driver = new FirefoxDriver(options);
			}
			
			driver.manage().window().maximize();
			driver.get(url);				
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);			
		}
		
		@After
		public void teardown() {
			driver.quit();
		}
	
		
		@Test
		public void test() throws Exception {
			LoginPage loginPage = PageFactory.initElements(driver, LoginPage.class);
			MenuPage menuPage = loginPage.logIn(user, pwd);
			driver.manage().timeouts().pageLoadTimeout(10, TimeUnit.SECONDS);
			
			Thread.sleep(2000);
			TestCasePage testCasePage = menuPage.click(menuPage.getButton_testCaseLink(), TestCasePage.class);
			testCasePage.selectProjectByName(projectName);
			
			Thread.sleep(2000);
			NewTestCasePopup newTestCasePopup = testCasePage.create(testCasePage.getNewTC());
			assertTrue("step 1 - Surbrillance du projet", testCasePage.getProjectBackgroundColor(projectName).contains("rgb(110, 172, 44)"));
			assertTrue("step 2 - presence des elements", newTestCasePopup.hasElementPresent());
			Thread.sleep(2000);
			newTestCasePopup.closePopup();
			testCasePage.moveAndClick(testCasePage.getRefresh());
			//Thread.sleep(2000);
			assertEquals("step 3 - nombre de cas de test sans ajout", tcNumber, testCasePage.getTcNumber().getText());
			testCasePage.create(testCasePage.getNewTC());
			newTestCasePopup.moveAndClick(newTestCasePopup.getAddTable());
			Thread.sleep(3000);
			newTestCasePopup.moveAndClick(newTestCasePopup.getConfirmAddTable());
			Thread.sleep(2000);
			newTestCasePopup.closePopup();
			assertEquals("step 3 - nombre de cas de test sans ajout", tcNumber, testCasePage.getTcNumber().getText());
			
			testCasePage.create(testCasePage.getNewTC());
			newTestCasePopup.sendText(newTestCasePopup.getTcName(), "steakhachefrite");
			newTestCasePopup.sendText(newTestCasePopup.getTcRef(), "888");
			newTestCasePopup.setTestCaseDescription("le gras c'est la vie");
			Thread.sleep(1000);
			newTestCasePopup.moveAndClick(newTestCasePopup.getAdd());
			Thread.sleep(1000);
			testCasePage.selectProjectByName(projectName);
			testCasePage.moveAndClick(testCasePage.getRefresh());
			Thread.sleep(1000);
			assertEquals("step 4 - nombre de cas de test apres ajout", "11", testCasePage.getTcNumber().getText());
			
			
		}

	}

