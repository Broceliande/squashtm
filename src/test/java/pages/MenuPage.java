package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class MenuPage {
	
	@FindBy(id = "test-case-link")
	private WebElement button_testCaseLink;
	
	protected WebDriver driver;
	protected HomePage homePage;
	
	public MenuPage(WebDriver driver) {
		this.driver = driver;
	}

	public HomePage getHomePage() {
		return homePage;
	}
	
	public WebElement getButton_testCaseLink() {
		return button_testCaseLink;
	}
		
	public <T extends MenuPage> T click(WebElement element, Class<T> page) {
		element.click();
		return PageFactory.initElements(driver, page);
	}

	public WebElement waitForClick(WebElement element, int time) {
		WebDriverWait wait = new WebDriverWait(driver, time);
		return wait.until(ExpectedConditions.elementToBeClickable(element));
	}
	
	public void moveAndClick(WebElement element) {
		Actions actions = new Actions(driver);
		actions.moveToElement(element).click().build().perform();
	}
	
	public WebElement sendText(WebElement element, String text) {
		element.sendKeys(text);
		return element;
	}
}
