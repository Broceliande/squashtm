package pages;

import java.util.Iterator;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class TestCasePage extends MenuPage {
	
	@FindBy(xpath = "//a[@id = 'tree-create-button']")
	private WebElement create;
	
	@FindBy(id = "ui-id-9")
	private WebElement newTC;
	
	@FindBy(xpath = "//div[@id='tree']/ul/li/a")
	List<WebElement> projects;
	
	@FindBy(xpath = "//input[@value='Rafra�chir']")
	private WebElement refresh;
	
	@FindBy(xpath = "//span[@class='dashboard-summary']/span[contains(@style, 'bold')]")
	private WebElement tcNumber;
	
	public TestCasePage(WebDriver driver) {
		super(driver);
	}
	
//	public List<WebElement> getProjects() {
//		List<WebElement> projectsList = driver.findElements(By.xpath("//div[@id='tree']/ul/li/a"));
//		return projectsList;	
//	}
	
	public WebElement getProjectByName(String name) {
		WebElement project = null;
		Iterator<WebElement> iterator = projects.iterator();
		while (iterator.hasNext()) {
			WebElement webElement = iterator.next();
			if(webElement.getText().contains(name)) {
				project = webElement;
				break;
			}
		}
		return project;
	}
	
	public void selectProjectByName(String name) {
		getProjectByName(name).click();
	}
	
	public String getProjectBackgroundColor(String name) throws InterruptedException {
		Actions actions = new Actions(driver);
		actions.moveToElement(getProjectByName(name)).click().moveByOffset(100, 0).build().perform();
		Thread.sleep(2000);
		String color = getProjectByName(name).getCssValue("background");
		return color;
	}
	
	public NewTestCasePopup create(WebElement tcType) {
		Actions actions = new Actions(driver);
		actions.moveToElement(waitForClick(create, 3)).click().build().perform();
		actions.moveToElement(waitForClick(tcType, 1)).click().build().perform();
		return PageFactory.initElements(driver, NewTestCasePopup.class);
	}
	
	public void name() {
		
	}

	public WebElement getNewTC() {
		return newTC;
	}

	public WebElement getRefresh() {
		return refresh;
	}

	public WebElement getTcNumber() {
		return tcNumber;
	}
}
