package pages;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class LoginPage {
	
	private WebDriver driver;
	
	@FindBy(id = "username")
	private WebElement input_username;
	@FindBy(id = "password")
	private WebElement input_password;
	@FindBy(xpath = "//input[@value='Se connecter']")
	private WebElement button_submit;
	
	public LoginPage(WebDriver driver) {
		this.driver = driver;
	}
	
	public MenuPage logIn(String user, String pwd) {
		input_username.clear();
		input_password.clear();
		input_username.sendKeys(user);
		input_password.sendKeys(pwd);
		button_submit.click();
		return PageFactory.initElements(driver, MenuPage.class);
	}

}
