package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchFrameException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class NewTestCasePopup extends TestCasePage{
	
	@FindBy(id = "add-test-case-name")
	private WebElement tcName;
	
	@FindBy(id = "add-test-case-reference")
	private WebElement tcRef;
	
	@FindBy(xpath = "//iframe[contains(@title,'add-test-case')]")
	private WebElement tcDescFrame;
	
	@FindBy(xpath = "//div[@id='add-test-case-dialog']/following-sibling::div//input[@type='button'][@value = 'Ajouter un autre']")
	private WebElement addAnother;
	
	@FindBy(xpath = "//div[@id='add-test-case-dialog']/following-sibling::div//input[@type='button'][@value = 'Ajouter']")
	private WebElement add;
	
	@FindBy(xpath = "//div[@id='add-test-case-dialog']/following-sibling::div//input[@type='button'][@value = 'Fermer']")
	private WebElement close;
	
	@FindBy(xpath = "//span[@id='ui-id-17']/following-sibling::a[.='close']")
	private WebElement closePopup;
	
	@FindBy(xpath = "//*[@id=\"cke_80\"]/span[@class='cke_button_icon cke_button__table_icon']")
	private WebElement addTable;
	
	@FindBy(xpath = "//a[@id='cke_198_uiElement']/..")
	private WebElement confirmAddTable;
	
	public NewTestCasePopup(WebDriver driver) {
		super(driver);
	}
	
	public boolean hasElementPresent() {
		boolean presence = false;
		if (tcName.isDisplayed()
				&& tcRef.isDisplayed()
				&& tcDescFrame.isDisplayed()
				&& addAnother.isDisplayed()
				&& add.isDisplayed()
				&& close.isDisplayed()) {
			presence = true;
		}
		return presence;
	}
	
	public void setTestCaseDescription(String desc) {
		
		try {
			WebElement element = driver.switchTo().frame(1).findElement(By.xpath("//html//body"));
			element.sendKeys(desc);
			}
			catch (NoSuchFrameException e) { 
			System.out.println(e.getMessage()); 
			}
			
			// Revenir au frame principal
			driver.switchTo().defaultContent();
	}
	

	
	public WebElement getTcName() {
		return tcName;
	}

	public WebElement getTcRef() {
		return tcRef;
	}

	public WebElement getTcDesc() {
		return tcDescFrame;
	}

	public WebElement getAddAnother() {
		return addAnother;
	}

	public WebElement getAdd() {
		return add;
	}

	public WebElement getClose() {
		return close;
	}

	public WebElement getClosePopup() {
		return closePopup;
	}

	public WebElement getAddTable() {
		return addTable;
	}

	public WebElement getConfirmAddTable() {
		return confirmAddTable;
	}

	public void closePopup() {
		closePopup.click();
	}

}
